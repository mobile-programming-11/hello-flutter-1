import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  const HelloFlutterApp({Key? key}) : super(key: key);

  @override
  State<HelloFlutterApp> createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "hola aleteo";
String thaishGreeting = "สวัสดี Flutter";
String japaneseshGreeting = "こんにちはフラッター";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = thaishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
              onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting
                      ? thaishGreeting
                      : englishGreeting;
                });
              },
              icon: Icon(Icons.refresh),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  displayText = displayText == japaneseshGreeting
                      ? thaishGreeting
                      : japaneseshGreeting;
                });
              },
              icon: Icon(Icons.ac_unit),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  displayText = displayText == spanishGreeting
                      ? thaishGreeting
                      : spanishGreeting;
                });
              },
              icon: Icon(Icons.accessibility_new),
            ),
          ],
        ),
        body: Center(
          child: Text(
            displayText,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}

// // class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(
//               onPressed: () {},
//               icon: Icon(Icons.refresh),
//             ),
//           ],
//         ),
//         body: Center(
//           child: Text(
//             "Hello flutter",
//             style: TextStyle(fontSize: 24),
//           ),
//         ),
//       ),
//     );
//   }
// }
